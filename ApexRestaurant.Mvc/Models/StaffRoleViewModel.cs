using System;
using System.ComponentModel.DataAnnotations;

namespace ApexRestaurant.Mvc.Models {
    public class StaffRoleViewModel {
        [Key]
        public int Staff_Roles_Id { get; set; }

        [Required]
        [StringLength (100)]

        public string Staff_Roles_Description { get; set; }
        public bool IsActive { get; set; }

        [ScaffoldColumn (false)]
        public string CreatedBy { get; set; }

        [ScaffoldColumn (false)]
        public DateTime CreatedOn { get; set; }

        [ScaffoldColumn (false)]
        public string UpdatedBy { get; set; }

        [ScaffoldColumn (false)]
        public DateTime UpdatedOn { get; set; }
    }
}