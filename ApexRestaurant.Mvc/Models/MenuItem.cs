using System;
using System.ComponentModel.DataAnnotations;

namespace ApexRestaurant.Mvc.Models {
    public class MenuItemViewModel {
        [Key]
        public int MenuItemId { get; set; }
        public int MenuId { get; set; }

        [Required]
        [StringLength (100)]
        public string Menu_Items_Name { get; set; }
        public bool IsActive { get; set; }

        [ScaffoldColumn (false)]
        public string CreatedBy { get; set; }

        [ScaffoldColumn (false)]
        public DateTime CreatedOn { get; set; }

        [ScaffoldColumn (false)]
        public string UpdatedBy { get; set; }

        [ScaffoldColumn (false)]
        public DateTime UpdatedOn { get; set; }
    }
}