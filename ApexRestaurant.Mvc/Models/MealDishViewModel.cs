using System;
using System.ComponentModel.DataAnnotations;

namespace ApexRestaurant.Mvc.Models {
    public class MealDishViewModel {
        [Key]
        public int MealDishesId { get; set; }
        public int MealId { get; set; }
        public int MenuItemId { get; set; }
        public string Quantity { get; set; }
        public bool IsActive { get; set; }
        [ScaffoldColumn (false)]
        public string CreatedBy { get; set; }

        [ScaffoldColumn (false)]
        public DateTime CreatedOn { get; set; }

        [ScaffoldColumn (false)]
        public string UpdatedBy { get; set; }

        [ScaffoldColumn (false)]
        public DateTime UpdatedOn { get; set; }
    }
}